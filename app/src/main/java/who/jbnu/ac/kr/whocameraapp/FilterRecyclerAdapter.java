package who.jbnu.ac.kr.whocameraapp;

import android.content.Context;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import who.jbnu.ac.kr.whocameraapp.Filter.FilterItem;

/**
 * Created by Yoon on 2016-04-28.
 */
public class FilterRecyclerAdapter extends RecyclerView.Adapter<FilterRecyclerAdapter.MyViewHolder>{
    Context context;

    ArrayList<FilterItem> filterItemArrayList;
    private static ClickListener clickListener;

    public FilterRecyclerAdapter( Context context,ArrayList<FilterItem> filterItemArrayList) {
        this.context = context;
        this.filterItemArrayList = filterItemArrayList;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(context).inflate(R.layout.filter_item,parent,false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
       holder.filter_name_textview.setText(filterItemArrayList.get(position).getName());
        holder.thum_img.setImageResource(R.mipmap.ic_launcher);
    }


    @Override
    public int getItemCount() {
        return filterItemArrayList.size();
    }



    public final static class MyViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener,View.OnLongClickListener{
        ImageView thum_img;
        TextView filter_name_textview;

        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
            thum_img = (ImageView)itemView.findViewById(R.id.thum_imageView);
            filter_name_textview = (TextView)itemView.findViewById(R.id.filter_name_textView);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(),v);
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onItemLongClick(getAdapterPosition(),v);
            return false;
        }
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        FilterRecyclerAdapter.clickListener = clickListener;
    }


    public interface ClickListener {
        void onItemClick(int position, View v);
        void onItemLongClick(int position, View v);
    }
}
