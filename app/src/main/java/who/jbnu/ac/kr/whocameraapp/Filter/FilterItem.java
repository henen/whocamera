package who.jbnu.ac.kr.whocameraapp.Filter;

import org.opencv.core.Mat;

/**
 * Created by Yoon on 2016-04-28.
 */
public abstract class FilterItem {
    String name;
    String thum_img;
    public abstract Mat setAlgorithm(Mat mat);
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getThum_img() {
        return thum_img;
    }

    public void setThum_img(String thum_img) {
        this.thum_img = thum_img;
    }

}
