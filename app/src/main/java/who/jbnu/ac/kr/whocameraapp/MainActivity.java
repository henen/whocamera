package who.jbnu.ac.kr.whocameraapp;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;

import who.jbnu.ac.kr.whocameraapp.Filter.BlurFilter;
import who.jbnu.ac.kr.whocameraapp.Filter.FilterItem;
import who.jbnu.ac.kr.whocameraapp.Filter.GassuinFilter;
import who.jbnu.ac.kr.whocameraapp.Filter.GrayFilter;
import who.jbnu.ac.kr.whocameraapp.Filter.LineFilter;
import who.jbnu.ac.kr.whocameraapp.Filter.ThreshFilter;
import who.jbnu.ac.kr.whocameraapp.Filter.MedianFilter;


public class MainActivity extends Activity implements CameraBridgeViewBase.CvCameraViewListener2 {
    private static final String TAG = "OCVSample::Activity";

    private CameraBridgeViewBase mOpenCvCameraView;
    private boolean              mIsJavaCamera = true;
    private MenuItem mItemSwitchCamera = null;
    RecyclerView filterRecyclerview;
    FilterRecyclerAdapter filterRecyclerAdapter;
    ArrayList<FilterItem> filterItemArrayList;
    int current_selelct_position = 0;
    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

//    public Tutorial1Activity() {
//        Log.i(TAG, "Instantiated new " + this.getClass());
//    }

    /** Called when the activity is first created. */



    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
       getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        filterRecyclerview = (RecyclerView)findViewById(R.id.recyclerview);
        filterRecyclerview.setLayoutManager(new LinearLayoutManager(MainActivity.this,LinearLayoutManager.HORIZONTAL,false));
        filterItemArrayList  = new ArrayList<>();
        filterItemArrayList.add(new GrayFilter());
        filterItemArrayList.add(new BlurFilter());
        filterItemArrayList.add(new ThreshFilter());
        filterItemArrayList.add(new LineFilter());
        filterItemArrayList.add(new GassuinFilter());
        filterItemArrayList.add(new MedianFilter());
        filterRecyclerAdapter = new FilterRecyclerAdapter(MainActivity.this,filterItemArrayList);
        filterRecyclerview.setAdapter(filterRecyclerAdapter);
        filterRecyclerAdapter.setOnItemClickListener(mClickListener);

        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.tutorial1_activity_java_surface_view);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCameraIndex(1);
        mOpenCvCameraView.setCvCameraViewListener(this);
//        mOpenCvCameraView.scale
    }

    FilterRecyclerAdapter.ClickListener mClickListener= new FilterRecyclerAdapter.ClickListener(){

        @Override
        public void onItemClick(int position, View v) {
            Log.v("henen","selected : "+position);
            current_selelct_position = position;
        }

        @Override
        public void onItemLongClick(int position, View v) {

        }
    };



    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }
    Mat mRgba;
    Mat mRgbaF;
    Mat mRgbaT;
    public void onCameraViewStarted(int width, int height) {

        mRgba = new Mat(height, width, CvType.CV_8UC4);
        mRgbaF = new Mat(height, width, CvType.CV_8UC4);
        mRgbaT = new Mat(width, width, CvType.CV_8UC4);

    }

    public void onCameraViewStopped() {
    }

    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        Mat mat;
//original
//        if(current_selelct_position==0){
//            mat =  inputFrame.rgba();
//        }else if(current_selelct_position==1){
//            mat =  inputFrame.gray();
//        }else if(current_selelct_position==2){
//            mat =  inputFrame.rgba();
//          //  Imgproc.GaussianBlur(mat,mat,new Size(11,11),0);
//        }else if(current_selelct_position==3){
//            mat =  inputFrame.rgba();
//            //Imgproc.medianBlur(mat,mat,3);
//        }else if(current_selelct_position==4){
//            mat =  inputFrame.rgba();
//            //Imgproc.blur(mat,mat,new Size(11,11));
//        }
//        else{
//            mat =  inputFrame.rgba();
//        }


//new

       mat = filterItemArrayList.get(current_selelct_position).setAlgorithm(inputFrame.rgba());


//        mRgba = inputFrame.rgba();
        //mRgba = mat;
        //mGray = inputFrame.gray();

        //rotate 90 degree
      //  Core.transpose(mRgba, mRgbaT);
       // Imgproc.resize(mRgbaT, mRgbaF, mRgbaF.size(), 0, 0, 0);
//        Imgproc.resize(mRgbaT, mRgbaF, new Size(500,500), 0, 0, 0);
       // Core.flip(mRgbaF, mRgba,0);
         Core.transpose(mat, mat);
//        Imgproc.resize(mat, mat, mRgbaF.size(), 0, 0, 0);
        Imgproc.resize(mat, mat, new Size(mRgbaF.size().width,mRgbaF.size().height), 0, 0, 0);
        Core.flip(mat, mat,0);

        return    mat;
    }

    public Mat rot90(Mat matImage, int rotflag){
        //1=CW, 2=CCW, 3=180
        Mat rotated = new Mat();
        if (rotflag == 1){
            rotated = matImage.t();
            Core.flip(rotated, rotated, 1); //transpose+flip(1)=CW
        } else if (rotflag == 2) {
            rotated = matImage.t();
            Core.flip(rotated, rotated,0); //transpose+flip(0)=CCW
        } else if (rotflag ==3){
            Core.flip(matImage, rotated,-1);    //flip(-1)=180
        } else if (rotflag != 0){ //if not 0,1,2,3:
            Log.e(TAG, "Unknown rotation flag("+rotflag+")");
        }
        return rotated;
    }

}