package who.jbnu.ac.kr.whocameraapp.Filter;

import org.opencv.android.CameraBridgeViewBase;
import org.opencv.core.Mat;

/**
 * Created by Yoon on 2016-04-28.
 */
public abstract class BaseAlgorithm {
    public abstract  Mat setAlgorithm(Mat mat);
}
