package who.jbnu.ac.kr.whocameraapp.Filter;

import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yoon on 2016-05-27.
 */
public class LineFilter extends FilterItem {
    int g_thresh=100;
    @Override
    public Mat setAlgorithm(Mat mat) {
        Mat hierarcy = new Mat();
        List<MatOfPoint> Contours = new ArrayList<MatOfPoint>();
        Imgproc.cvtColor(mat,mat,Imgproc.COLOR_RGB2GRAY);
        Imgproc.threshold(mat,mat,g_thresh,255,Imgproc.THRESH_BINARY);
        Imgproc.findContours(mat,Contours,hierarcy,Imgproc.RETR_LIST,Imgproc.CHAIN_APPROX_SIMPLE);
        return mat;
    }
    public LineFilter() {
        super.setName("라인");
        super.setThum_img("test.png");
    }
}
