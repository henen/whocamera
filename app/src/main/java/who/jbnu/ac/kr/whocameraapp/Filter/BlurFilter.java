package who.jbnu.ac.kr.whocameraapp.Filter;

import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

/**
 * Created by Yoon on 2016-05-27.
 */
public class BlurFilter extends  FilterItem {
    public BlurFilter() {
        super.setName("Blur");
        super.setThum_img("test.png");
    }

    @Override
    public Mat setAlgorithm(Mat mat) {
        Imgproc.blur(mat,mat,new Size(11,11));
        return mat;
    }
}
