package who.jbnu.ac.kr.whocameraapp.Filter;

import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

/**
 * Created by Yoon on 2016-05-27.
 */

public class GrayFilter extends  FilterItem {
    @Override
    public Mat setAlgorithm(Mat mat) {
        Imgproc.cvtColor(mat,mat,Imgproc.COLOR_RGB2GRAY);
        return mat;
    }
    public GrayFilter() {
        super.setName("그레이");
        super.setThum_img("test.png");
    }
}
