package who.jbnu.ac.kr.whocameraapp.Filter;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

/**
 * Created by Yoon on 2016-05-27.
 */
public class ThreshFilter extends  FilterItem {
    int g_thresh=100;
    @Override
    public Mat setAlgorithm(Mat mat) {
        Imgproc.cvtColor(mat,mat,Imgproc.COLOR_RGB2GRAY);
        Imgproc.threshold(mat,mat,g_thresh,255,Imgproc.THRESH_BINARY);
        return mat;
    }
    public ThreshFilter() {
        super.setName("Thresh");
        super.setThum_img("test.png");
    }
}