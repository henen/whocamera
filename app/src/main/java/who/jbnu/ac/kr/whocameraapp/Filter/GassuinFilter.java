package who.jbnu.ac.kr.whocameraapp.Filter;

import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

/**
 * Created by Yoon on 2016-05-27.
 */
public class GassuinFilter extends  FilterItem{

    @Override
    public Mat setAlgorithm(Mat mat) {
        Imgproc.GaussianBlur(mat,mat,new Size(11,11),0);
        return mat;
    }

    public GassuinFilter() {
        super.setName("Gaussion");
        super.setThum_img("test.png");
    }


}
